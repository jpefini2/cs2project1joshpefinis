package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	private static final String ERROR_LOG_LOG = "readErrors.log";
	public static final String FIELD_SEPARATOR = ",";
	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws FileNotFoundException File not found. 
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		
		try (Scanner input = new Scanner(this.inventoryFile)) {
			File errorLog = new File(ERROR_LOG_LOG);
			if (!errorLog.exists()) {
				errorLog.createNewFile();
			}
			
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);

				try {
					Automobile auto = this.makeAutomobile(fields);
					autos.add(auto);
				} catch (Exception e) {
					System.err.println("Error reading file: empty String on line: " + line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return autos;  
	}

	private Automobile makeAutomobile(String[] fields) {
		String make = fields[0];
		String model = fields[1];
		int year = Integer.parseInt(fields[2]);
		double miles = Double.parseDouble(fields[3]);
		double price = Double.parseDouble(fields[4]);
		return new Automobile(make, model, year, miles, price);
	}

	private String[] splitLine(String line, String fieldSeparator) {
		return line.split(fieldSeparator);
	}
	
	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and 
	 * sorts them into the matching dealership in the given dealership group.
	 * If there is no matching dealership, one is created.
	 * 
	 * @precondition dealershipGroup != null
	 * @postcondition none
	 * 
	 * @param dealershipGroup the DealershipGroup to sort into
	 * 
	 * @throws FileNotFoundException File not found. 
	 */
	public void loadAutosIntoDealershipGroup(DealershipGroup dealershipGroup) throws FileNotFoundException {
		if (dealershipGroup == null) {
			throw new IllegalArgumentException("dealershipGroup cannt be null");
		}
		try (Scanner input = new Scanner(this.inventoryFile)) {
			File errorLog = new File(ERROR_LOG_LOG);
			if (!errorLog.exists()) {
				errorLog.createNewFile();
			}
			
			FileWriter errorWriter = new FileWriter(errorLog, true);
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);
				
				try {
					String dealershipName = fields[0];
					Automobile newAuto = new Automobile(fields[1], fields[2], Integer.parseInt(fields[3]), Double.parseDouble(fields[4]), Double.parseDouble(fields[5]));
					
					if (dealershipGroup.findDealership(dealershipName) == null) {
						dealershipGroup.addDealership(dealershipName);
						dealershipGroup.addAuto(dealershipName, newAuto);
						dealershipGroup.addAuto("ALL AUTOS", newAuto);
					} else {
						dealershipGroup.addAuto(dealershipName, newAuto);
						dealershipGroup.addAuto("ALL AUTOS", newAuto);
					}
				} catch (Exception e) {
					errorWriter.write("Error reading file: empty String on line: " + line + System.lineSeparator());	
				}	
			} errorWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}

