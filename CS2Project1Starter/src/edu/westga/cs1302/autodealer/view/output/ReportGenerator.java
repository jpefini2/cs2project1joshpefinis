package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {
	
	private NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param dealer The dealership to build summary report for
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Dealership dealer) {
		String summary = "";
		
		Inventory inventory = dealer.getInventory();

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = dealer.getName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			summary += System.lineSeparator();
			summary += this.buildMostAndLeastExpensiveSummaryOutput(inventory);
			
			summary += System.lineSeparator();
			summary += this.buildAverageMilesReport(inventory);
			
			summary += System.lineSeparator();
			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(3000, inventory);

			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(10000, inventory);
		}

		return summary;
	}

	private String buildAverageMilesReport(Inventory inventory) {
		String report = "Average miles: ";
		double averageMiles = inventory.computeAverageMiles();
		
		DecimalFormat milesFormat = new DecimalFormat("#,###.000");
		report += milesFormat.format(averageMiles);
		
		return report;
	}

	private String buildMostAndLeastExpensiveSummaryOutput(Inventory inventory) {
		Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
		Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();

		String report = "Most expensive auto: ";
		report += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
		
		report += "Least expensive auto: ";
		report += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
		
		return report;
	}

	private String buildPriceSegmentBreakdownSummary(double priceSegmentRange, Inventory inventory) {
		int[] autoPriceSegmentsCount = inventory.countVehiclesInEachPriceSegment(priceSegmentRange);
		
		if (autoPriceSegmentsCount == null) {
			return "";
		}

		String priceSegmentSummary = "Vehicles in " + this.currencyFormatter.format(priceSegmentRange) + " segments" + System.lineSeparator();
		
		double startingPrice = 0;
		double endingPrice = priceSegmentRange;
		
		for (int i = 0; i < autoPriceSegmentsCount.length; i++) {
			String autoPriceSegmentCountAsterisks = "";
			
			if (autoPriceSegmentsCount[i] == 0) {
				autoPriceSegmentCountAsterisks = "0";
			} else {
				for (int asteriskCount = 0; asteriskCount < autoPriceSegmentsCount[i]; asteriskCount++) {
					autoPriceSegmentCountAsterisks = autoPriceSegmentCountAsterisks + "*";
				}
			}
			
			String startingPriceCurrencyFormat = this.currencyFormatter.format(startingPrice);
			String endingPriceCurrencyFormat = this.currencyFormatter.format(endingPrice);
			priceSegmentSummary += startingPriceCurrencyFormat + " - " + endingPriceCurrencyFormat + " : " + autoPriceSegmentCountAsterisks + System.lineSeparator();
			
			startingPrice = endingPrice + 0.01;
			endingPrice = (priceSegmentRange * (i + 2)); 
			autoPriceSegmentCountAsterisks = "";
		}
		
		return priceSegmentSummary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		String output = this.currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}
	
	/**
	 * Builds a report of all the autos of the specified make at the specified
	 * dealership
	 * 
	 * @precondition make != null, dealership !=null
	 * @postcondition none
	 * 
	 * @param make The make to search the dealership's inventory for
	 * @param dealership The dealership to search through
	 *
	 * @return A formatted String report of the dealership's inventory
	 */
	public String buildMakeDealershipReport(String make, Dealership dealership) {
		String report = "Autos by " + make + " at " + dealership.getName() + ":" + System.lineSeparator();
		
		int matchCount = 0;
		double totalMatchPrice = 0.0;
		
		for (Automobile current : dealership.getInventory().getAutos()) {
			if (make.toLowerCase().equals(current.getMake().toLowerCase())) {
				report = report + this.buildIndividualAutomobileReport(current) + System.lineSeparator();
				matchCount++;
				totalMatchPrice += current.getPrice();
			}
		}
		
		double averagePriceDouble = totalMatchPrice / matchCount;
		String averagePrice = this.currencyFormatter.format(averagePriceDouble);
	
		report = report + "Average Price: " + averagePrice + System.lineSeparator();
		
		if (matchCount == 0) {
			report = "No " + make + "s at " + dealership.getName();
		}
		
		return report;
		
		
		
		
		
	}

}
