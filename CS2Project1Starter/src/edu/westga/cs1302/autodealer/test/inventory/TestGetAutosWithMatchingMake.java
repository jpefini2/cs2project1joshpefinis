package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestGetAutosWithMatchingMake {
	
	private static final String FIESTA = "Fiesta";
	private static final String FUSION = "Fusion";
	private static final String FOCUS = "Focus";
	private static final String FORD = "Ford";

	@Test
	void testNullMake() {
		Inventory inventory = new Inventory();
		
		Automobile auto0 = new Automobile(FORD, FOCUS, 2018, 580.0, 5000.0);
		inventory.add(auto0);
		
		assertThrows(IllegalArgumentException.class,
				()-> inventory.getAutosWithMatchingMake(null));
		
	}

	@Test
	void testAllAutosMatch() {
		Inventory inventory = new Inventory();
		
		Automobile auto0 = new Automobile(FORD, FOCUS, 2018, 580.0, 5000.0);
		inventory.add(auto0);
		Automobile auto1 = new Automobile(FORD, FUSION, 2010, 900.0, 6000.0);
		inventory.add(auto1);
		Automobile auto2 = new Automobile(FORD, FIESTA, 2014, 2460.0, 50340.0);
		inventory.add(auto2);
		
		assertEquals(3, inventory.getAutosWithMatchingMake(FORD).size());
	}
	
	@Test
	void testNoAutosMatch() {
		Inventory inventory = new Inventory();
		
		Automobile auto0 = new Automobile(FORD, FOCUS, 2018, 580.0, 5000.0);
		inventory.add(auto0);
		Automobile auto1 = new Automobile(FORD, FUSION, 2010, 900.0, 6000.0);
		inventory.add(auto1);
		Automobile auto2 = new Automobile(FORD, FIESTA, 2014, 2460.0, 50340.0);
		inventory.add(auto2);
		
		assertEquals(0, inventory.getAutosWithMatchingMake("Jeep").size());
	}
	
	@Test
	void testSomeAutosMatch() {
		Inventory inventory = new Inventory();
		
		Automobile auto0 = new Automobile("Toyota", "Corrolla", 2018, 580.0, 5000.0);
		inventory.add(auto0);
		Automobile auto1 = new Automobile(FORD, FUSION, 2010, 900.0, 6000.0);
		inventory.add(auto1);
		Automobile auto2 = new Automobile(FORD, FIESTA, 2014, 2460.0, 50340.0);
		inventory.add(auto2);
		
		assertEquals(2, inventory.getAutosWithMatchingMake(FORD).size());
	}
}
